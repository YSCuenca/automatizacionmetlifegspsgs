package com.qvision.metlife.steps;

import com.qvision.metlife.pages.DeclaracionSolicitdGspMetLifePage;

public class DeclaracionSolicitdGspMetLifeSteps {
	
	DeclaracionSolicitdGspMetLifePage declaracion = new DeclaracionSolicitdGspMetLifePage();
	
	public void gestioneDeclaracion() {
		declaracion.gestioneDeclaracion();
	}
}
