package com.qvision.metlife.steps;

import com.qvision.metlife.pages.EstiloVidySalSolicitudGspMetLifePage;

public class EstiloVidySalSolicitudGspMetLifeSteps {

	EstiloVidySalSolicitudGspMetLifePage vidaSalud = new EstiloVidySalSolicitudGspMetLifePage();
	
	public void gestioneInformacionEstiloVida(String fuerzaArmada, String practicaDeporte, String viajePiloto, 
			String viajePasajero, String vidaF,
			String pasajero, String consumoDrogas, String fumarAlgo, String beberAlcohol, String procesoJudicial) {
		
		vidaSalud.gestioneInformacionEstiloVida(fuerzaArmada, practicaDeporte, viajePiloto, viajePasajero, vidaF, pasajero, 
				consumoDrogas, fumarAlgo, beberAlcohol, procesoJudicial);
	}
	
	public void gestioneInformacionEstadoSalud(String embarazo, String dudaEnfermedad, String dolencia, 
			String tratamientos, String rayosX, String enfermedadMas, String herencia, String sida, 
			String sexual, String amputacion, String colesterol, String hipertension, String cancer, 
			String diabetes, String epilepsia, String nariz, String boca, String oidos, String ojos, 
			String vejiga, String depresion, String hepatetica, String columna, String pulmones, 
			String infarto, String sistemaSeguridad, String perdidaPeso, String peso, String estatura) {
		
		vidaSalud.gestioneInformacionEstadoSalud(embarazo, dudaEnfermedad, dolencia, tratamientos, rayosX, enfermedadMas,
				herencia, sida, sexual, amputacion, colesterol, hipertension, cancer, diabetes, epilepsia, nariz, boca, 
				oidos, ojos, vejiga, depresion, hepatetica, columna, pulmones, infarto, sistemaSeguridad, perdidaPeso, peso, 
				estatura);
	}
}
