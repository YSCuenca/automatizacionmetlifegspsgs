package com.qvision.metlife.steps;

import com.qvision.metlife.pages.TomadorPolizaInfoBasicaSolicitudGspMetLifePage;

public class TomadorPolizaSolicitudGspMetLifeSteps {

	TomadorPolizaInfoBasicaSolicitudGspMetLifePage tomador;
	
	public void gestioneInformacionTomadorPoliza(String aseguradoIgualTomar, String parentesco, String tipoTomador, String primerNombreT, 
			String segundoNombreT, String tercerNombreT, String primerApellioT, String segundoApellidoT, String generoT, String fechaNacimientoT, 
			String tipoIdentificacionT, String numeroIdentificacionT, String paisNacimiento, String deptoNacimiento, String ciudadNacimiento, 
			String nacionalidadT, String estadoCivilT, String direccionResidenciaT, String paisResidenciaT, String deptoResidenciaT, String ciudadResidenciaT, 
			String telefonoResidenciaT, String celularT, String correoElectronicoT, String comunicacionMetL) {
		
		tomador.gestioneInformacionTomadorPoliza(aseguradoIgualTomar, parentesco, tipoTomador, primerNombreT, 
				segundoNombreT, tercerNombreT, primerApellioT, segundoApellidoT, generoT, fechaNacimientoT, 
				tipoIdentificacionT, numeroIdentificacionT, paisNacimiento, deptoNacimiento, ciudadNacimiento, 
				nacionalidadT, estadoCivilT, direccionResidenciaT, paisResidenciaT, deptoResidenciaT, ciudadResidenciaT, 
				telefonoResidenciaT, celularT, correoElectronicoT, comunicacionMetL);

	};
	
	public void gestioneInformacionFinancieraT(String tipoActividad, String nombreEmpresa, String actividadEconomicaEmpT, String nitEmpresaT,
			String direccionTrabajoT, String paisTrabajoT, String deptoTrabajoT, String ciudadTrabajoT, String telefonoEmpreT, String ocupacionT, 
			String actividadEconPrinT, String ActiEconSecundariaT, String recursosPubliT, String poderPublicoT, String reconocimientoPublicT, 
			String vinculo, String ingresosMensualesT, String otrosIngresosT, String totalActivos, String paisOrigenIngresosT, String egresosMensualesT, 
			String totalPasivosT, String origenFondosT, String transaMonedaExtr, String indemSeguros) {
		
		tomador.gestioneInformacionFinancieraT(tipoActividad, nombreEmpresa, actividadEconomicaEmpT, nitEmpresaT,
				direccionTrabajoT, paisTrabajoT, deptoTrabajoT, ciudadTrabajoT, telefonoEmpreT, ocupacionT, 
				actividadEconPrinT, ActiEconSecundariaT, recursosPubliT, poderPublicoT, reconocimientoPublicT, 
				vinculo, ingresosMensualesT, otrosIngresosT, totalActivos, paisOrigenIngresosT, egresosMensualesT, 
				totalPasivosT, origenFondosT, transaMonedaExtr, indemSeguros);
	}
	
}
