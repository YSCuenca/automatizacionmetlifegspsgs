package com.qvision.metlife.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.pages.PageObject;

public class Esperas extends PageObject {
	WebDriverWait esperaExp;
	public static WebDriver driver;
	
	public Esperas(){
		if(driver == null) {
			driver = getDriver();
		}
	}

	public void esperaExpEleVisible(String idElemento, int tiempoEspera) {
		if(driver == null) {
			driver = getDriver();
		}
		esperaExp = new WebDriverWait(driver, tiempoEspera);
		esperaExp.until(ExpectedConditions.visibilityOfElementLocated(By.id(idElemento)));
	}

	public void esperaExpEleHabilitado(String idElemento, int tiempoEspera) {
		if(driver == null) {
			driver = getDriver();
		}
		try{
			esperaExp = new WebDriverWait(driver, tiempoEspera);
		}catch(Exception e){
			try {
				Thread.sleep(10000);
				esperaExp = new WebDriverWait(driver, tiempoEspera);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		esperaExp.until(ExpectedConditions.elementToBeClickable(By.id(idElemento)));
		

	}
	
	public void esperaExpEleVisibleByXpath(String xpath, int tiempoEspera) {
		if(driver == null) {
			driver = getDriver();
		}
		esperaExp = new WebDriverWait(driver, tiempoEspera);
		esperaExp.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}

	public void esperaExpEleHabilitadoByXpath(String xpath, int tiempoEspera) {
		if(driver == null) {
			driver = getDriver();
		}
		esperaExp = new WebDriverWait(driver, tiempoEspera);
		esperaExp.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));

	}

	public void esperaSimple(int tiempo) {
		try {
			Thread.sleep(tiempo);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
