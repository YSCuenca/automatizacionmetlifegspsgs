package com.qvision.metlife.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class BeneficiariosSolicitudGspMetlifePage extends PageObject{

	Esperas espera;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[1]/div[1]/select")
	WebElementFacade sltTipoB;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[1]/div[2]/select")
	WebElementFacade sltTipoIdenti;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[1]/div[3]/div/input")
	WebElement txtNumDocument;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[2]/div[1]/input")
	WebElement txtFirstName;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[2]/div[2]/input")
	WebElement txtSecondName;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[2]/div[3]/input")
	WebElement txtThreethName;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[2]/div[4]/input")
	WebElement txtFirstSurname; 
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[2]/div[5]/input")
	WebElement txtSecondSurname; 
	
	@FindBy(xpath = "//*[@id=\"popUpContentDiv\"]/div[1]/div[3]/div/select")
	WebElementFacade sltRelaWithAse;
	
	@FindBy(xpath = "//*[@id=\"popUpContentDiv\"]/div[1]/div[4]/div/div/div/input")
	WebElement txtFechaNacimiento;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[4]/div[4]/div[1]/select")
	WebElementFacade sltPlan;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[4]/div[4]/div[2]/input")
	WebElement txtPorcentajePoliza;	
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[4]/div[3]/label[2]/input")
	WebElement rdbMale;
	
	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[4]/div[3]/label[3]/input")
	WebElement rdbFemale;
	
	@FindBy(xpath = "//*[@id=\"popUpContentDiv\"]/div[2]/button[1]")
	WebElement btnGuardar;
	
	@FindBy(xpath = "//*[@id=\"application-beneficary-footer\"]/div/button[3]")
	WebElement btnContinuar;
	
	@FindBy(xpath = "//*[@id=\"addBeneficiary\"]/button[2]")
	WebElement newBtnBeneficiario;
	
	public void agregarBeneficiario(String tipoBeneficiario, String tipoIdentificacion, 
			String numDocument, String firstName, String secondName, String threethName, 
			String firstSurname, String secondSurname, String relaWithAse, String fechaNacimientoT, String porcentajeP,
			String plan, String generoB, String id) {
		
		newBtnBeneficiario.click();
		espera.esperaExpEleHabilitadoByXpath("/html/body/section/div/div[3]/div/div/div/form/div[3]/div[13]/div/div/div/div[2]/div[1]/div[1]/div[1]/select", 10);
		sltTipoB.selectByVisibleText(tipoBeneficiario);
		sltTipoIdenti.selectByVisibleText(tipoIdentificacion);
		espera.esperaSimple(1000);
		txtNumDocument.sendKeys(numDocument);
		txtFirstName.sendKeys(firstName);
		txtSecondName.sendKeys(secondName);
		txtThreethName.sendKeys(threethName);
		txtFirstSurname.sendKeys(firstSurname);
		txtSecondSurname.sendKeys(secondSurname);
		espera.esperaSimple(1000);
		try {
			sltRelaWithAse.selectByVisibleText(relaWithAse);
		} catch (Exception e) {
			espera.esperaSimple(5000);
		}
		
		JavascriptExecutor js = (JavascriptExecutor) getDriver();  
		js.executeScript("arguments[0].value = arguments[1]", txtFechaNacimiento, fechaNacimientoT);
	
		sltPlan.selectByVisibleText(plan);
		txtPorcentajePoliza.sendKeys(porcentajeP);
	
		
		if(generoB.equals("M")) {
			rdbMale.click();
		}else {
			rdbFemale.click();
		}
		espera.esperaSimple(1000);
		btnGuardar.click();
		
		espera.esperaSimple(1000);
		if(generoB.equals("F")) {
			espera.esperaSimple(3000);
			btnContinuar.click();
			espera.esperaExpEleHabilitado("noTsix", 30);
			espera.esperaSimple(3000);
		}
	}
}
