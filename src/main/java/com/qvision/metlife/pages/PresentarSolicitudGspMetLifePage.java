package com.qvision.metlife.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PresentarSolicitudGspMetLifePage extends PageObject {

	Esperas espera = new Esperas();
	public static String idSolicitud;
	
	@FindBy(id = "fileType")
	WebElementFacade sltTypeFile;
	
	@FindBy(xpath= "//*[@id=\"application_uploadDocuments_Documents\"]/div[2]/div/div[2]/div")
	WebElement parentBtnSendFile;
	
	//WebElement btnSendFile2 = parentBtnSendFile.findElement(By.id("fileUploadBtn"));

	@FindBy(xpath = "/html/body/section/div/div[3]/div/div/div/div/form[1]/div[3]/div[2]/div/span")
	WebElement spanIdSolicitud;

	@FindBy(id = "btnUploadDocumetsSaveLater")
	WebElement btnSaveLeave;

	public void presentarSolicitud(String pathIdenti, String pathCertificado, String pathFirma) {

		String[][] documentType = new String[3][3];
		documentType[0][0] = "Identificación";
		documentType[0][1] = pathIdenti;

		documentType[1][0] = "Certificado de Origen Ingresos";
		documentType[1][1] = pathCertificado;

		documentType[2][0] = "Firma del cliente";
		documentType[2][1] = pathFirma;

		for (int i = 0; i < 3; i++) {

			espera.esperaExpEleHabilitado("fileType", 30);
			sltTypeFile.selectByVisibleText(documentType[i][0]);

			espera.esperaSimple(5000);
			
			WebElement btnSubirDoc = getDriver().findElement(By.id("fileUploadBtn"));
			/*
			 * JavascriptExecutor js = (JavascriptExecutor) getDriver();
			 * evaluateJavascript("document.getElementById('fileUploadBtn').click()");
			 * js.executeScript("document.getElementById('fileUploadBtn').click()");
			 * js.executeScript("document.getElementById(arguments[0]).click()",
			 * "fileUploadBtn");
			 * js.executeScript("document.getElementById('arguments[0]').click()",
			 * "fileUploadBtn");
			 */
			btnSubirDoc.click();
			espera.esperaSimple(10000);
			
		    Robot robot = null;
			try {
				robot = new Robot();
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			StringSelection selection = new StringSelection(documentType[i][1]);
		    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection,null);
		    robot.keyPress(KeyEvent.VK_CONTROL);
		    robot.keyPress(KeyEvent.VK_V);
		    robot.keyRelease(KeyEvent.VK_CONTROL);
		    robot.keyRelease(KeyEvent.VK_V);
		    robot.setAutoDelay(2000);

		    robot.keyPress(KeyEvent.VK_ENTER);
		    robot.keyRelease(KeyEvent.VK_ENTER); 
			
			/*
			 * espera.esperaSimple(1000);
			 * sltTypeFile.selectByVisibleText(documentType[i][0]);
			 * btnSendFile.sendKeys(documentType[i][1]);
			 */
		}

		idSolicitud = spanIdSolicitud.getText();

		espera.esperaSimple(500);
		btnSaveLeave.click();
		espera.esperaSimple(120000);
	}


}
