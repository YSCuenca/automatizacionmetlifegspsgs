package com.qvision.metlife.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PagoSolicitudGspMetLifePage extends PageObject {

	Esperas espera = new Esperas();
	
	@FindBy(id = "annual")
	WebElement btnPrimaTAnual;

	@FindBy(id = "semiannual")
	WebElement btnPrimaTSemiAnual;

	@FindBy(id = "monthly")
	WebElement btnPrimaTMensual;

	@FindBy(xpath = "//*[@id=\"application_PaymentDetails_Footer\"]/div/div/button[3]")
	WebElement btnContinuar;
	
	@FindBy(id = "paymentMethodCheckBox")
	WebElementFacade sltFormaPago;
	
	@FindBy(id = "paymentRecuuringSelectDiv0")
	WebElementFacade sltFormaPagoRecu;
	
	@FindBy(id = "refundPaymentSelectDiv")
	WebElementFacade sltDevolucion;

	public void gestionePago(String lapsoTiempo, String formaPago, String formaPagoRecu, String devolucion) {
		
		espera.esperaExpEleHabilitado("annual", 30);
		WebDriver driver = getDriver();
		
		switch (lapsoTiempo) {
		case "anual":
			btnPrimaTAnual.click();
			break;
		case "semestral":
			btnPrimaTSemiAnual.click();
			break;
		case "mensual":
			btnPrimaTMensual.click();
			break;
		}
		
		espera.esperaExpEleHabilitado("paymentMethodCheckBox", 10);
		sltFormaPago.selectByVisibleText(formaPago);
		espera.esperaExpEleHabilitado("paymentRecuuringSelectDiv0", 10);
		sltFormaPagoRecu.selectByVisibleText(formaPagoRecu);
		espera.esperaExpEleHabilitado("refundPaymentSelectDiv", 10);
		sltDevolucion.selectByVisibleText(devolucion);
		
		String windowHandled = driver.getWindowHandle();
		espera.esperaSimple(500);
		btnContinuar.click();
		driver.switchTo().alert().accept();
		espera.esperaSimple(1000);
		driver.switchTo().window(windowHandled);
		espera.esperaSimple(1000);
		btnContinuar.click();
	}
}
