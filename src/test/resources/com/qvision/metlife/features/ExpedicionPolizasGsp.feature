# language: en
# encoding: iso-8859-1
Feature: Expedir una poliza
As a user
I want expedir una poliza

  Scenario: Expedici�n de p�lizas en Gsp
    Given que quiero expedir una nueva p�liza en Gsp
    When inicie sesi�n en Gsp
      | usuario | contrase�a |
      | 4000002 | metlife1   |
    Then valido la sesion en Gsp
    And cree un nuevo cliente
      | primerNombre | segundoNombre | tercerNombre | primerApellido | segundoApellido | fechaNacimiento | nacionalidad | genero   | clienteFumador | estadoCivil | ocupacion                            | tipoIdentificacion | numeroIdentificacion | ingresosMensuales | correoElectronico  | direccion      | paisResidencia | departamento | ciudad      | tipoTelefono1 | telefono1 | tipoTelefono2 | telefono2  |
      |   Karla      |               |              | Hermanides     |                 |25/02/1983|  			 COLOMBIA     | Femenino | No             | Soltero     | Agente inmobiliario (Administrativo) | Cedula             |60000303207			  	 |           5000000 | lgaaxcia@gmail.com | Cra 67 N�36-52 | COLOMBIA       | CUNDINAMARCA | BOGOTA D.C. | Casa          |   2558943 | Celular       | 3295566789 |
    And valido la creaci�n del nuevo cliente
    And realice el an�lisis de necesidades del cliente
      | salarioDeseado | ingresoMensual | semanasCotizadas | edadPension |
      |        3000000 |       25000000 |              580 |          65 |
    And Cree una nueva cotizaci�n
      | numeroIdentificacion | primaPlan1 | primaPlan2 | primaPlan3 | primaplan4 | 
      |          60000303197 |   80000000 |  100000000 |  100000000 | 120000000 |
    And Cree una nueva solicitud
      | paisNacimiento | deptoNacimiento | ciudadNacimiento | ciudadanoEU | poderTranferenciaEU | comunicacionMetLife | infoAdicional1 | infoAdicional2 | infoAdicional3 |
      | COLOMBIA       | CUNDINAMARCA    | BOGOTA D.C.      | NO          | NO                  | SI                  | NO             | NO             | NO             |
    And Gestione la informaci�n b�sica del tomador de la p�liza
      | aseguradoIgualTomar | parentesco | tipoTomador | primerNombreT | segundoNombreT | tercerNombreT | primerApellioT | segundoApellidoT | generoT | fechaNacimientoT | tipoIdentificacionT | numeroIdentificacionT | paisNacimiento | deptoNacimiento | ciudadNacimiento | nacionalidadT | estadoCivilT | direccionResidenciaT | paisResidenciaT | deptoResidenciaT | ciudadResidenciaT | telefonoResidenciaT | celularT   | correoElectronicoT | comunicacionMetL | direccionCorrespondenciaT | paisCorrespondenciaT | deptoCorrespondenciaT | ciudadCorrespondenciaT |
      | SI                  | C�nyuge    | Natural     | Juan          | Jose           |               | Luna           | Mora             | M       |01/08/1981|  Cedula                     |            1001245678 | COLOMBIA       | CUNDINAMARCA    | BOGOTA D.C.      | COLOMBIA      | Uni�n Libre  | Cr 1 N 24 -57        | COLOMBIA        | CUNDINAMARCA     | BOGOTA D.C.       |             4562932 | 3210716543 | juana@gmail.com    | SI               | Cr 23 N 7 -56      			 |						 COLOMBIA | CUNDINAMARCA    		  | BOGOTA D.C.       		 |  
    And Gestione la informaci�n financiera del tomador de la p�liza
      | tipoActividad | nombreEmpresa | actividadEconomicaEmpT | nitEmpresaT | direccionTrabajoT | paisTrabajoT | deptoTrabajoT | ciudadTrabajoT | telefonoEmpreT | ocupacionT | actividadEconPrinT | ActiEconSecundariaT | recursosPubliT | poderPublicoT | reconocimientoPublicT | vinculo | ingresosMensualesT | otrosIngresosT | totalActivos | paisOrigenIngresosT | egresosMensualesT | totalPasivosT | origenFondosT             | transaMonedaExtr | indemSeguros |
      | Asalariado    | Sas           | Banco Central          |      923414 | Calle 28 N 19 -23 | COLOMBIA     | CUNDINAMARCA  | BOGOTA D.C.    |        4321898 | Economista | Asalariados        | NO                  | NO             | NO            | NO                    | NO      |            7000000 |              0 |    150000000 | COLOMBIA            |           4000000 |      60000000 | De la actividad principal | NO               | NO           |
    And Agregue los beneficiarios
	    |id| tipoBeneficiario | tipoIdentificacion | numDocument | firstName | secondName | threethName | firstSurname | secondSurname | relaWithAse | fechaNacimientoT | porcentajeP  | plan                | generoB |
	    |0|  Contingente      | Cedula             | 1223786363  | Arley     |            |             | Carrasca     |               | Hijo(a)     |01/08/2005| 100                  | Vida Entera 60 a�os | M       |
	    |1|  Primario         | Cedula             | 1223786364  | Camila    |            |             | Cortazar     |               | Hermano(a)  |01/08/1990| 100                  | Vida Entera 60 a�os | F       |
    And Gestione el estilo de vida y salud del cliente
    	| fuerzaArmada | practicaDeporte | viajePiloto | viajePasajero | vidaF | pasajero | consumoDrogas | fumarAlgo | beberAlcohol | procesoJudicial |estatura | peso | perdidaPeso | sistemaSeguridad | infarto | pulmones | columna | hepatetica | depresion | vejiga | ojos | oidos | boca | nariz | epilepsia | diabetes | cancer | hipertension | colesterol | amputacion | sexual | sida | herencia | enfermedadMas | rayosX | tratamientos | dolencia | dudaEnfermedad | embarazo |
      |     NO       |        NO       |   NO        |           NO  |  NO   | NO       | NO            | NO        | NO           | NO              |    180  | 65   |      NO     |     NO           | NO      | NO       | NO      | NO         | NO        |     NO | NO   |   NO  |  NO  |  NO   | NO        | NO       | NO     | NO           |    NO      |     NO     |    NO  |   NO | NO       | NO            | NO     | NO           | NO       |    NO          | NO       |  
    And Agregue la informaci�n financiera del cliente
     | tipoActividad | nitEmpresaT | nombreEmpresa | empleadoMetlife | actividadEconoEmp | direccionTrabajoT | paisTrabajoT | deptoTrabajoT | ciudadTrabajoT | telefonoEmpreT |        ocupacionT                    |           actividadEconPrinT              | ActiEconSecundariaT | recursosPubliT | poderPublicoT | reconocimientoPublicT | vinculo | ingresosMensualesT | otrosIngresosT | paisOrigenIngresosT | totalActivos | egresosMensualesT | totalPasivosT | origenFondosT             | transaMonedaExtr | indemSeguros |
     | Asalariado    |      923414 | Choklitos SOS |            NO   | Edici�n de libros | Calle 28 N 19 -23 | COLOMBIA     | CUNDINAMARCA  | BOGOTA D.C.    |        4321898 | Agente inmobiliario (Administrativo) | Actividades de administraci�n empresarial | NO                  | NO             | NO            | NO                    | NO      |            8000000 |      0         |  COLOMBIA           |  150000000   |       4000000     |      60000000 | De la actividad principal | NO               | NO           |
    And Confirme la solicitud
    And Gestione la declaraci�n del cliente
    And Gestione el pago de la solicitud
     |lapsoTiempo|  formaPago   | formaPagoRecu| devolucion |
     |	anual	   | Pago directo | Pago directo | Cheque		  |
    And Presente la solicitud
     |																																					pathIdenti																						  | 																											pathCertificado																																					| 																																	pathFirma																											 |
     |C:\\Users\\user\\Desktop\\AutomatizacionMetLifeGspSgs\\src\\test\\resources\\com\\qvision\\metlife\\documents\\Identificacion.pdf | C:\\Users\\user\\Desktop\\AutomatizacionMetLifeGspSgs\\src\\test\\resources\\com\\qvision\\metlife\\documents\\Certificado O. de Ingresos.pdf | C:\\Users\\user\\Desktop\\AutomatizacionMetLifeGspSgs\\src\\test\\resources\\com\\qvision\\metlife\\documents\\Firma Cliente.pdf |
    Then Valido la creaci�n de la solicitud
